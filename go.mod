module bitbucket.org/pbisse/eventserver

go 1.14

require (
	github.com/go-playground/validator/v10 v10.2.0
	github.com/google/uuid v1.1.1
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/lib/pq v1.5.2
	github.com/prometheus/client_golang v1.6.0
)
